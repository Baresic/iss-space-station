import { useEffect, useState } from "react";
import useInterval from "./hooks/useInterval";
import "./App.css";

const geolocationUrl = (lat, long) => {
  return `https://api.bigdatacloud.net/data/reverse-geocode?latitude=${lat}&longitude=${long}&localityLanguage=en&key=aca29f0e5f994e98bcf48d2723c20d0b`;
};

const App = () => {
  const [issLocation, setIssLocation] = useState("Searching...");
  const triggerSearch = () => {
    //redirect value from public folder beacuse it is http call and netlify is https
    fetch(`/api`)
      .then((response) => response.json())
      .then((station) =>
        fetch(
          geolocationUrl(
            station?.iss_position?.latitude,
            station?.iss_position?.longitude
          )
        )
      )
      .then((response) => response.json())
      .then((location) => {
        if (!location?.continent || location.continent === "") {
          setIssLocation(location?.locality);
          return;
        }
        setIssLocation(
          `${location?.principalSubdivision}, ${location?.countryName}`
        );
      });
  };
  useInterval(() => {
    triggerSearch();
  }, 18000);

  useEffect(() => {
    triggerSearch();
  }, []);
  return (
    <div className="App">
      <h2>ISS current location</h2>
      <p>{issLocation}</p>
    </div>
  );
};

export default App;
